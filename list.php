
<?php

  include('db.php');
 // used INNER JOIN to list insted of numbers for valuta, full name
  $sql = "SELECT konverzije.id, konverzije.time_stamp, konverzije.iznos, konverzije.valuta, konverzije.iznos_kursa,konverzije.kurs ,konverzije.konvertovani_iznos, valuta.naziv FROM konverzije INNER JOIN valuta ON konverzije.valuta =valuta.id ORDER BY konverzije.time_stamp ASC ";
  $result = mysqli_query($connection,$sql) or die(mysql_error());


  if ($result->num_rows > 0) {

    $json_response = array();
    while($row = $result->fetch_assoc()) {
        $datum = $row['time_stamp'];
        $datum_temp = explode('-', $datum);
        $datum_new = ($datum_temp[2].'-'.$datum_temp[1].'-'.$datum_temp[0]);

        if (!isset($json_response[ $datum_new ])) {
            $json_response[ $datum_new ] = [
                'datum' => $datum_new,
                'Konverzije' => [],
            ];
        }
        $json_response[ $datum_new ]['Konverzije'][] = [

                'iznos' => $row['iznos'],
                'valuta' => $row['naziv'],
                'kurs' => $row['kurs'],
                'Iznos kursa' => $row['iznos_kursa'],
                'Konvertovana vrednost' => $row['konvertovani_iznos']
        ];
    }
    // We want the final result to ignore the keys and to create a JSON array not a JSON object
    $data = [];
    foreach ($json_response as $element) {
        $data[] = $element;
    }
}
print '<pre>'.json_encode($data, JSON_PRETTY_PRINT).'</pre>';
//echo json_encode($data, JSON_PRETTY_PRINT);
