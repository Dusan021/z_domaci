-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 18, 2018 at 03:37 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `KURSNA_LISTA`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzije`
--

CREATE TABLE `konverzije` (
  `id` int(11) NOT NULL,
  `time_stamp` date NOT NULL,
  `iznos` int(11) NOT NULL,
  `valuta` varchar(30) NOT NULL,
  `iznos_kursa` int(11) NOT NULL,
  `kurs` varchar(50) NOT NULL,
  `konvertovani_iznos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzije`
--

INSERT INTO `konverzije` (`id`, `time_stamp`, `iznos`, `valuta`, `iznos_kursa`, `kurs`, `konvertovani_iznos`) VALUES
(5, '2018-06-15', 54235, '1', 119, 'prodajni', 6429397),
(8, '2018-06-15', 324, '1', 118, 'srednji', 38295),
(9, '2018-06-15', 324, '2', 102, 'prodajni', 33180),
(10, '2018-06-15', 123, '2', 102, 'kupovni', 12521),
(11, '2018-06-15', 123, '1', 118, 'kupovni', 14494);

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

CREATE TABLE `kurs_dinara` (
  `id` int(11) NOT NULL,
  `datum` date NOT NULL,
  `valuta` int(11) NOT NULL,
  `kupovni` float NOT NULL,
  `srednji` float NOT NULL,
  `prodajni` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta`, `kupovni`, `srednji`, `prodajni`) VALUES
(19, '2018-06-15', 0, 117.838, 118.193, 118.547),
(20, '2018-06-15', 0, 101.795, 102.101, 102.408),
(21, '2018-06-15', 0, 102.024, 102.331, 102.638),
(22, '2018-06-15', 0, 134.903, 135.309, 135.715),
(23, '2018-06-15', 0, 76.0393, 76.2681, 76.4969),
(24, '2018-06-15', 0, 77.5505, 77.7839, 78.0173),
(25, '2018-06-15', 0, 11.6408, 11.6758, 11.7108),
(26, '2018-06-15', 0, 15.8144, 15.862, 15.9096),
(27, '2018-06-15', 0, 12.5105, 12.5481, 12.5857),
(28, '2018-06-15', 0, 0.918242, 0.921005, 0.923768),
(29, '2018-06-15', 0, 1.6287, 1.6336, 1.6385),
(30, '2018-06-15', 0, 15.8679, 15.9156, 15.9633),
(31, '2018-06-15', 0, 0, 15.9996, 0),
(32, '2018-06-15', 0, 0, 337.693, 0),
(33, '2018-06-15', 0, 0, 27.5964, 0),
(34, '2018-06-15', 0, 0, 4.5902, 0),
(35, '2018-06-15', 0, 0, 0.365955, 0),
(36, '2018-06-15', 0, 0, 60.4309, 0),
(37, '2018-06-15', 1, 117.838, 118.193, 118.547),
(38, '2018-06-15', 2, 101.795, 102.101, 102.408),
(39, '2018-06-15', 0, 102.024, 102.331, 102.638),
(40, '2018-06-15', 0, 134.903, 135.309, 135.715),
(41, '2018-06-15', 0, 76.0393, 76.2681, 76.4969),
(42, '2018-06-15', 0, 77.5505, 77.7839, 78.0173),
(43, '2018-06-15', 0, 11.6408, 11.6758, 11.7108),
(44, '2018-06-15', 0, 15.8144, 15.862, 15.9096),
(45, '2018-06-15', 0, 12.5105, 12.5481, 12.5857),
(46, '2018-06-15', 0, 0.918242, 0.921005, 0.923768),
(47, '2018-06-15', 0, 1.6287, 1.6336, 1.6385),
(48, '2018-06-15', 0, 15.8679, 15.9156, 15.9633),
(49, '2018-06-15', 0, 0, 15.9996, 0),
(50, '2018-06-15', 0, 0, 337.693, 0),
(51, '2018-06-15', 0, 0, 27.5964, 0),
(52, '2018-06-15', 0, 0, 4.5902, 0),
(53, '2018-06-15', 0, 0, 0.365955, 0),
(54, '2018-06-15', 0, 0, 60.4309, 0);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

CREATE TABLE `valuta` (
  `id` int(11) NOT NULL,
  `naziv` varchar(30) NOT NULL,
  `kod` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'Euro', 'eur'),
(2, 'Americki Dolar', 'usd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `konverzije`
--
ALTER TABLE `konverzije`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurs_dinara`
--
ALTER TABLE `kurs_dinara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valuta`
--
ALTER TABLE `valuta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `konverzije`
--
ALTER TABLE `konverzije`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kurs_dinara`
--
ALTER TABLE `kurs_dinara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `valuta`
--
ALTER TABLE `valuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
